# test-cpp-dynamic-tuple

C++ の std::tuple を動的に生成するテスト。

## 動機

自作ライブラリのテストのため、
C++ のテンプレート関数に渡す引数をファイルから読み込めるようにしたいという目的を叶えるため、
C++ の tuple を動的に生成することを試みています。

- [既存の否定的な意見](https://stackoverflow.com/questions/44394094/dynamically-creating-and-expanding-stdtuple-into-parameter-pack)
